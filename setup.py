from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='module',
    version='0.1.0',
    description='Module package',
    long_description=readme,
    author='Pedo Fdez',
    author_email='pedringcoding@gmail.com',
    url='https://gitlab.com/pedringcoding/python-project-template',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)